// ## Теоретический вопрос

// 1. Опишите каким образом можно создать новый HTML на странице.
// document.createElement('Название тега')

// 2. Опишите что означает первый параметр функции insertAdjacentHTML и опишите возможные варианты этого параметра.
// Первый параметр означает куда мы будем вставлять
// 'beforebegin': до самого element (до открывающего тега).
// 'afterbegin': сразу после открывающего тега element (перед первым потомком).
// 'beforeend': сразу перед закрывающим тегом element (после последнего потомка).
// 'afterend': после element (после закрывающего тега).

// 3. Как можно удалить элемент со страницы?
// С помощью метода .remove




// - Создать функцию, которая будет принимать на вход массив и опциональный второй аргумент parent - DOM-элемент, к которому будет прикреплен список (по дефолту должен быть document.body).
// - Каждый из элементов массива вывести на страницу в виде пункта списка;

function listCreated(array, parent= document.body){
    let list = document.createElement('ul')
    parent.prepend(list)
    for (let i = 0; i < array.length ; i++) {
        let listItem = document.createElement('li')
        listItem.textContent = array[i]
        list.append(listItem)
    }
}

listCreated(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv, 'Herson", 'Kiev'], document.querySelector("div"))
listCreated(["1", "2", "3", "sea", "user", 23])
// Один список в боди один в диве
